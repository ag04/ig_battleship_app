import './GameLobby.css';
import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import * as R from 'ramda';
import {Link} from 'react-router-dom';
import {toastr} from 'react-redux-toastr';
import {fetchPlayers} from '../../redux/players/actions';
import Player from '../../containers/Player/Player';
import Game from '../../containers/Game/Game';
import {fetchGamesByPlayer, setGameStatusParams} from '../../redux/games/actions';

class GameLobby extends React.Component {
    static propTypes = {
        fetchPlayers: PropTypes.func,
        games: PropTypes.arrayOf(
            PropTypes.shape({
                gameId: PropTypes.number.isRequired,
                opponentId: PropTypes.number.isRequired,
                status: PropTypes.string.isRequired
            }).isRequired
        ),
        gamesByPlayerFetchError: PropTypes.string.isRequired,
        handleFetchGames: PropTypes.func,
        handleSetGameStatusParams: PropTypes.func.isRequired,
        players: PropTypes.arrayOf(
            PropTypes.shape({
                email: PropTypes.string.isRequired,
                id: PropTypes.number.isRequired,
                name: PropTypes.string.isRequired
            })
        ),
        playersFetchError: PropTypes.string.isRequired,
        selectedPlayer: PropTypes.number.isRequired
    };

    componentDidMount() {
        this.props.fetchPlayers();
    }

    displayPlayers() {
        if (this.props.playersFetchError) {
            toastr.error('Fetch error', this.props.playersFetchError);
            return <h3>Player fetch error!</h3>;
        }
        return this.props.players.map(({id, name, email}) =>
            <Player
                email={email} id={id} key={id}
                name={name} onClick={this.props.handleFetchGames}
                selected={id === this.props.selectedPlayer}
            />
        );
    }

    displayGames() {
        if (this.props.gamesByPlayerFetchError) {
            return <h3>Games by player fetch error!</h3>;
        }
        const {games} = this.props;
        if (R.isEmpty(games) && this.props.selectedPlayer) {
            return <h3>No games found for player with ID: {this.props.selectedPlayer}</h3>;
        }
        return games.map(
            ({gameId, opponentId, status}) =>
                <Game
                    id={gameId} key={gameId} onClick={this.props.handleSetGameStatusParams}
                    opponentId={opponentId} playerId={this.props.selectedPlayer} status={status}
                />
        );
    }

    render() {
        return (
            <div className="game-lobby">
                <Link className="my-link" to="/createPlayer">Create player</Link>
                <br/>
                <Link className="my-link" to="/createGame">Create game</Link>
                <h2>List of players</h2>
                <div className="list-of-players">
                    {this.displayPlayers()}
                </div>
                <div className="list-of-games">
                    <br/>
                    {this.props.selectedPlayer && !R.isEmpty(this.props.games) ?
                        <h3>Games by player with ID: {this.props.selectedPlayer}</h3> : null}
                    {this.displayGames()}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    games: state.games.games,
    gamesByPlayerFetchError: state.games.gamesByPlayerFetchError,
    players: state.players.players,
    playersFetchError: state.players.playersFetchError,
    selectedPlayer: state.games.selectedPlayer
});

const mapDispatchToProps = (dispatch) => ({
    fetchPlayers: () => dispatch(fetchPlayers()),
    handleFetchGames: (playerId) => dispatch(fetchGamesByPlayer(playerId)),
    handleSetGameStatusParams: (gameId, opponentId) => dispatch(setGameStatusParams(gameId, opponentId))
});

export default connect(mapStateToProps, mapDispatchToProps)(GameLobby);
