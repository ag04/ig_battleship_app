import React from 'react';
import './GameStatus.css';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {toastr} from 'react-redux-toastr';
import Board from '../../containers/Board/Board';
import {clearGameStatusMessages, fetchGameStatus, fireSalvo} from '../../redux/games/actions';
import Legend from '../../containers/Legend/Legend';
import logo from '../../logo.svg';
import {convertIntegersToLocations} from '../../util/Util';

class GameStatus extends React.Component {
    static propTypes = {
        clearGameStatusMessages: PropTypes.func.isRequired,
        fetchGameStatus: PropTypes.func.isRequired,
        fireSalvo: PropTypes.func.isRequired,
        fireSalvoError: PropTypes.string.isRequired,
        fireSalvoSuccess: PropTypes.string.isRequired,
        gameStatus: PropTypes.object.isRequired,
        gameStatusFetchError: PropTypes.string.isRequired,
        gameStatusFetchSuccess: PropTypes.string.isRequired,
        selectedGame: PropTypes.number.isRequired,
        selectedPlayer: PropTypes.number.isRequired,
        selectedSquares: PropTypes.array.isRequired
    };

    componentDidMount() {
        this.props.fetchGameStatus(this.props.selectedPlayer, this.props.selectedGame);
    }

    componentWillUnmount() {
        this.props.clearGameStatusMessages();
    }

    handleFireSalvo = () => {
        this.props.fireSalvo(this.props.selectedPlayer, this.props.selectedGame, convertIntegersToLocations(this.props.selectedSquares));
    };

    handleChangePlayerTurn = () => {
        this.props.fetchGameStatus(this.props.selectedPlayer, this.props.selectedGame);
    };

    render() {
        if (this.props.gameStatusFetchSuccess) {
            const {gameStatus: {self, opponent, game}} = this.props;
            return (
                <div className="game-status">
                    <Link className="my-link" to="/">Back to player list</Link>
                    <br/>
                    <h3>Game ID: {this.props.selectedGame}</h3>
                    {game.won ?
                        <h4 style={{background: '#ff0078'}}>Player (ID: {game.won})
                            won! </h4> : <h4>Player (ID: {game.playerTurn}) turn.</h4>}
                    <div className="board-wrapper">
                        <h4>Board - Player ID: {self.playerId}</h4>
                        <h4>Remaining ships: {self.remainingShips}</h4>
                        <Board editable={false} squares={self.board}/>
                    </div>
                    <div className="board-wrapper">
                        <h4>Board - Opponent ID: {opponent.playerId}</h4>
                        <h4>Remaining ships: {opponent.remainingShips}</h4>
                        <Board editable={true} squares={opponent.board}/>
                    </div>
                    <div>
                        {this.props.fireSalvoError ? <h3>Fire salvo error!</h3> : null}
                    </div>
                    {this.props.fireSalvoSuccess ?
                        <div>
                            <button onClick={this.handleChangePlayerTurn} style={{marginBottom: '3%'}}>Next turn</button>
                        </div> :
                        <div>
                            <button onClick={this.handleFireSalvo} style={{marginBottom: '3%'}}>Fire Salvo</button>
                        </div>
                    }
                    <Legend/>
                </div>
            );
        } else if (this.props.gameStatusFetchError) {
            toastr.error('Fetch error', this.props.gameStatusFetchError);
            return <h2>{this.props.gameStatusFetchError}</h2>;
        }
        return <img alt="logo" className="App-logo" src={logo}/>;
    }
}

const mapStateToProps = (state) => ({
    fireSalvoError: state.games.fireSalvoError,
    fireSalvoSuccess: state.games.fireSalvoSuccess,
    gameStatus: state.games.gameStatus,
    gameStatusFetchError: state.games.gameStatusFetchError,
    gameStatusFetchSuccess: state.games.gameStatusFetchSuccess,
    selectedGame: state.games.selectedGame,
    selectedPlayer: state.games.selectedPlayer,
    selectedSquares: state.games.salvoShots
});

const mapDispatchToProps = (dispatch) => ({
    clearGameStatusMessages: () => dispatch(clearGameStatusMessages()),
    fetchGameStatus: (playerId, gameId) => dispatch(fetchGameStatus(playerId, gameId)),
    fireSalvo: (playerId, gameId, salvo) => dispatch(fireSalvo(playerId, gameId, salvo))
});

export default connect(mapStateToProps, mapDispatchToProps)(GameStatus);
