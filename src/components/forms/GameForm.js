import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {fetchPlayers} from '../../redux/players/actions';
import {clearGameFormState, createGame} from '../../redux/forms/actions';
import './GameForm.css';
import {toastr} from 'react-redux-toastr';

class GameForm extends React.Component {
    static propTypes = {
        clearGameFormStatusMessages: PropTypes.func.isRequired,
        createGame: PropTypes.func.isRequired,
        fetchPlayers: PropTypes.func.isRequired,
        gameCreateError: PropTypes.string.isRequired,
        gameCreateSuccess: PropTypes.string.isRequired,
        players: PropTypes.array.isRequired
    };
    constructor(props) {
        super(props);

        this.state = {
            challengerId: '',
            opponentId: ''
        };
    }

    componentDidMount() {
        this.props.fetchPlayers();
    }

    componentWillUnmount() {
        this.props.clearGameFormStatusMessages();
    }

    handleSubmit = (event) => {
        event.preventDefault();
        this.props.createGame(this.state.challengerId, this.state.opponentId);
    };

    handleChallengerChange = (event) => {
        this.setState({challengerId: event.target.value});
    };

    handleOpponentChange = (event) => {
        this.setState({opponentId: event.target.value});
    };

    getPlayerList = () => this.props.players.map(({id, name, email}) =>
        <option key={id} value={id}>{name} ({email})</option>);

    render() {
        return (
            <div className="game-form">
                <Link className="my-link" to="/">Back to player list</Link>
                <form onSubmit={this.handleSubmit}>
                    <label htmlFor="selectChallenger">Select challenger:</label>
                    <select id="selectChallenger" name="selectChallenger" onChange={this.handleChallengerChange}>
                        <option value="null">--Select player--</option>
                        {this.getPlayerList()}
                    </select>
                    <br/>
                    <label htmlFor="selectOpponent">Select opponent:</label>
                    <select id="selectOpponent" name="selectOpponent" onChange={this.handleOpponentChange}>
                        <option value="null">--Select player--</option>
                        {this.getPlayerList()}
                    </select>
                    <br/>
                    <input type="submit" value="Create game"/>
                </form>
                {this.props.gameCreateError ?
                    <div>
                        <h3>{this.props.gameCreateError}</h3>
                        {toastr.error('Create error', this.props.gameCreateError)}
                    </div> : null}
                {this.props.gameCreateSuccess}
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    gameCreateError: state.forms.gameCreateError,
    gameCreateSuccess: state.forms.gameCreateSuccess,
    players: state.players.players
});

const mapDispatchToProps = (dispatch) => ({
    clearGameFormStatusMessages: () => dispatch(clearGameFormState()),
    createGame: (challengerId, opponentId) => dispatch(createGame(challengerId, opponentId)),
    fetchPlayers: () => dispatch(fetchPlayers())
});

export default connect(mapStateToProps, mapDispatchToProps)(GameForm);
