import React from 'react';
import './PlayerForm.css';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {clearPlayerFormState, createPlayer} from '../../redux/forms/actions';
import {toastr} from 'react-redux-toastr';

class PlayerForm extends React.Component {
    static propTypes = {
        clearPlayerFormStatusMessages: PropTypes.func,
        createPlayer: PropTypes.func,
        playerCreateError: PropTypes.string.isRequired,
        playerCreateSuccess: PropTypes.string.isRequired
    };

    state = {
        email: '',
        name: ''
    };

    componentWillUnmount() {
        this.props.clearPlayerFormStatusMessages();
    }

    handleChangeName = (event) => {
        this.setState({name: event.target.value});
    };

    handleChangeEmail = (event) => {
        this.setState({email: event.target.value});
    };

    handleSubmit = (event) => {
        event.preventDefault();

        this.props.createPlayer(this.state.name, this.state.email);
    };

    render() {
        return (
            <div className="player-form">
                <Link to="/">Back to player list</Link>
                <form onSubmit={this.handleSubmit}>
                    <div>
                        <label htmlFor="playerName">Player name: </label>
                        <br/>
                        <input id="playerName" name="name" onChange={this.handleChangeName} type="text"/>
                    </div>
                    <div>
                        <label htmlFor="playerEmail">Player email: </label>
                        <br/>
                        <input id="playerEmail" name="email" onChange={this.handleChangeEmail} type="text"/>
                    </div>
                    <div>
                        <br/>
                        <input type="submit" value="Create player"/>
                    </div>
                </form>
                {this.props.playerCreateError ?
                    <div>
                        <h3>{this.props.playerCreateError}</h3>
                        {toastr.error('Create error', this.props.playerCreateError)}
                    </div> : null}
                {this.props.playerCreateSuccess}
            </div>
        );
    }
}

const mapStateToPros = (state) => ({
    playerCreateError: state.forms.playerCreateError,
    playerCreateSuccess: state.forms.playerCreateSuccess
});

const mapDispatchToProps = (dispatch) => ({
    clearPlayerFormStatusMessages: () => dispatch(clearPlayerFormState()),
    createPlayer: (name, email) => dispatch(createPlayer(name, email))
});

export default connect(mapStateToPros, mapDispatchToProps)(PlayerForm);
