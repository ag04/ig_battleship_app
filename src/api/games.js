import api from './api';

export const fetchGameList = (playerId) => api.get(`${playerId}/game/list`);

export const fetchGameStatus = (playedId, gameId) => api.get(`${playedId}/game/${gameId}`);

export const fireSalvo = (playerId, gameId, salvo) => api.put(`${playerId}/game/${gameId}`, {salvo});
