import axios from 'axios';
import applyConverters from 'axios-case-converter';

export const API_URL = 'http://localhost:8080/battleship-api/player/';

export default applyConverters(axios.create({
    baseURL: API_URL,
    timeout: 10000
}));
