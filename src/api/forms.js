import api from './api';

export const createPlayer = (name, email) => api.post('', {email, name});

export const createGame = (challengerId, opponentId) => api.post(`${opponentId}/game`, {playerId: challengerId});
