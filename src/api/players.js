import api from './api';

export const fetchPlayerList = () => api.get('list');
