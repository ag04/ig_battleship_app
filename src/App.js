import React, {Component} from 'react';
import './App.css';
import {Route, Switch} from 'react-router';
import ReduxToastr from 'react-redux-toastr';
import GameLobby from './components/GameLobby/GameLobby';
import PlayerForm from './components/forms/PlayerForm';
import GameForm from './components/forms/GameForm';
import GameStatus from './components/GameStatus/GameStatus';
import 'react-redux-toastr/lib/css/react-redux-toastr.min.css';

class App extends Component {
    render() {
        return (
            <div className="App">
                <h1>Battleship App</h1>
                <Switch>
                    <Route component={GameLobby} exact={true} path="/"/>
                    <Route component={PlayerForm} path="/createPlayer"/>
                    <Route component={GameForm} path="/createGame"/>
                    <Route component={GameStatus} path="/game/:id"/>
                </Switch>
                <ReduxToastr
                    closeOnToastrClick={true}
                    newestOnTop={false}
                    position="top-center"
                    preventDuplicates={true}
                    progressBar={true}
                    timeOut={2000}
                    transitionIn="bounceIn"
                    transitionOut="bounceOutUp"
                />
            </div>
        );
    }
}

export default App;
