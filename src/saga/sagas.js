import {FETCH_PLAYERS, fetchPlayersError, setPlayers} from '../redux/players/actions';
import {fetchGameList, fetchGameStatus, fireSalvo} from '../api/games';
import {fetchPlayerList} from '../api/players';
import {
    FETCH_GAME_STATUS, FETCH_GAMES_BY_PLAYER, fetchGamesByPlayerError, fetchGameStatusError,
    FIRE_SALVO, fireSalvoError, fireSalvoSuccess, setGames, setGameStatus
} from '../redux/games/actions';
import {
    CREATE_GAME, CREATE_PLAYER, createGameError,
    createGameSuccess, createPlayerError, createPlayerSuccess
} from '../redux/forms/actions';
import {createGame, createPlayer} from '../api/forms';
import {all, call, put, takeEvery} from 'redux-saga/effects';

export function* fetchPlayersSaga() {
    try {
        const {data: {players}} = yield call(fetchPlayerList);
        yield put(setPlayers(players));
    } catch (error) {
        yield put(fetchPlayersError(error));
    }
}

export function* fetchGamesByPlayerSaga(action) {
    try {
        const {data: {games}} = yield call(fetchGameList, action.playerId);
        yield put(setGames(games, action.playerId));
    } catch (error) {
        yield put(fetchGamesByPlayerError(error));
    }
}

export function* createPlayerSaga(action) {
    try {
        yield call(createPlayer, action.name, action.email);
        yield put(createPlayerSuccess());
    } catch (error) {
        yield put(createPlayerError(error));
    }
}

export function* createGameSaga(action) {
    try {
        yield call(createGame, action.challengerId, action.opponentId);
        yield put(createGameSuccess());
    } catch (error) {
        yield put(createGameError(error));
    }
}

export function* fetchGameStatusSaga(action) {
    try {
        const {data} = yield call(fetchGameStatus, action.playerId, action.gameId);
        yield put(setGameStatus(data));
    } catch (error) {
        yield put(fetchGameStatusError(error));
    }
}

export function* fireSalvoSaga(action) {
    try {
        const {data: {salvo}} = yield call(fireSalvo, action.playerId, action.gameId, action.salvo);
        yield put(fireSalvoSuccess(salvo));
    } catch (error) {
        yield put(fireSalvoError(error));
    }
}

export function* mySagas() {
    yield all([
        takeEvery(FETCH_PLAYERS, fetchPlayersSaga),
        takeEvery(FETCH_GAMES_BY_PLAYER, fetchGamesByPlayerSaga),
        takeEvery(CREATE_PLAYER, createPlayerSaga),
        takeEvery(CREATE_GAME, createGameSaga),
        takeEvery(FETCH_GAME_STATUS, fetchGameStatusSaga),
        takeEvery(FIRE_SALVO, fireSalvoSaga)
    ]);
}
