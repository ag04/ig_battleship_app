import {mySagas} from './sagas';
import {all, call} from 'redux-saga/effects';

export function* mainSaga() {
    try {
        yield all([
            call(mySagas)
        ]);
    } catch (error) {
        console.log('error: ', error);
    }
}
