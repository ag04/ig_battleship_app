import React from 'react';
import './Player.css';
import PropTypes from 'prop-types';

export default class Player extends React.PureComponent {
    static propTypes = {
        email: PropTypes.string,
        id: PropTypes.number,
        name: PropTypes.string,
        onClick: PropTypes.func,
        selected: PropTypes.bool
    };

    handleClick = () => {
        const {onClick, id} = this.props;
        onClick(id);
    };

    render() {
        const {name, email, id, selected} = this.props;
        const classes = ['player'];
        if (selected) {
            classes.push('selected');
        }
        return <div className={classes.join(' ')} onClick={this.handleClick}>
            <h3>Player ID: {id}</h3>
            <h4>Player: {name}</h4>
            <h4>Email: {email}</h4>
        </div>;
    }
}
