import React from 'react';
import './Board.css';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {toastr} from 'react-redux-toastr';
import Square from '../Square/Square';
import {toggleSquareForSalvo} from '../../redux/games/actions';
import {convertLocationToInteger} from '../../util/Util';

class Board extends React.Component {
    static propTypes = {
        editable: PropTypes.bool.isRequired,
        gameStatus: PropTypes.object.isRequired,
        salvoShotResults: PropTypes.object.isRequired,
        selectedPlayer: PropTypes.number.isRequired,
        selectedSquares: PropTypes.array.isRequired,
        squares: PropTypes.array.isRequired,
        toggleSquareForSalvo: PropTypes.func.isRequired
    };

    handleClick(location, editable) {
        const {gameStatus: {game, self}} = this.props;
        const {selectedSquares} = this.props;
        if (editable && game.won) {
            toastr.info('Game finished!', `Player (ID: ${game.won}) won!`);
        } else if (editable && this.props.selectedPlayer !== game.playerTurn) {
            toastr.info('Not your turn!', `Player (ID: ${game.playerTurn}) is on turn.`);
        } else if (editable && selectedSquares.length < self.remainingShips) {
            this.props.toggleSquareForSalvo(location);
        } else if (editable && selectedSquares.length === self.remainingShips && selectedSquares.includes(location)) {
            this.props.toggleSquareForSalvo(location);
        }
    }

    getBoard() {
        const rows = [];
        for (let row = 0; row < 10; row += 1) {
            rows.push(<div className="board-row" key={row}>
                {this.getRow(row)}
            </div>);
        }

        return rows;
    }

    getRow(row) {
        const squares = [];
        for (let col = 0; col < 10; col += 1) {
            squares.push(this.getSquare(row, col));
        }

        return squares;
    }

    getSquare(row, col) {
        return <Square
            editable={this.props.editable} key={row * 10 + col}
            onClick={() => this.handleClick(row * 10 + col, this.props.editable)}
            salvoResult={this.checkIfSquareInSalvoResult(row * 10 + col)}
            selectedForSalvo={this.props.selectedSquares.includes(row * 10 + col)}
            value={this.props.squares[row][col]}
        />;
    }

    checkIfSquareInSalvoResult(location) {
        const {salvoShotResults} = this.props;
        for (const key in salvoShotResults) {
            if (convertLocationToInteger(key) === location && salvoShotResults[key] === 'KILL') {
                return 'KILL';
            } else if (convertLocationToInteger(key) === location && salvoShotResults[key] === 'HIT') {
                return 'HIT';
            } else if (convertLocationToInteger(key) === location && salvoShotResults[key] === 'MISS') {
                return 'MISS';
            }
        }
        return 'NOT';
    }

    render() {
        return (
            <div className="board">
                {this.getBoard()}
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    gameStatus: state.games.gameStatus,
    salvoShotResults: state.games.salvoResult,
    selectedPlayer: state.games.selectedPlayer,
    selectedSquares: state.games.salvoShots
});

const mapDispatchToProps = (dispatch) => ({toggleSquareForSalvo: (squareId) => dispatch(toggleSquareForSalvo(squareId))});

export default connect(mapStateToProps, mapDispatchToProps)(Board);
