import React from 'react';
import Square from '../Square/Square';

function Legend() {
    return (
        <div className="legend">
            <div>
                <Square value="#"/>
                <p>Ship</p>
            </div>
            <div>
                <Square value="X"/>
                <p>Hit</p>
            </div>
            <div>
                <Square value="0"/>
                <p>Miss</p>
            </div>
            <div>
                <Square editable={true} selectedForSalvo={true} value="S"/>
                <p>Salvo</p>
            </div>
            <div style={{width: '20%'}}>
                <Square value="."/>
                <p>Empty/unknown</p>
            </div>
            <br/>
            <div>
                <Square editable={true} salvoResult="KILL"/>
                <p>Kill</p>
            </div>
            <div>
                <Square editable={true} salvoResult="HIT"/>
                <p>Hit</p>
            </div>
            <div>
                <Square editable={true} salvoResult="MISS"/>
                <p>Miss</p>
            </div>
        </div>
    );
}

export default Legend;
