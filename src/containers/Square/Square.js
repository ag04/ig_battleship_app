import React from 'react';
import './Square.css';
import PropTypes from 'prop-types';

function Square(props) {
    Square.propTypes = {
        editable: PropTypes.bool,
        onClick: PropTypes.func,
        salvoResult: PropTypes.string,
        selectedForSalvo: PropTypes.bool,
        value: PropTypes.string
    };

    const classes = ['square'];
    if (props.editable && props.salvoResult === 'KILL') {
        classes.push('salvo-result-kill');
    } else if (props.editable && props.salvoResult === 'HIT') {
        classes.push('salvo-result-hit');
    } else if (props.editable && props.salvoResult === 'MISS') {
        classes.push('salvo-result-miss');
    } else if (props.editable && props.selectedForSalvo) {
        classes.push('salvo-shot');
    } else {
        switch (props.value) {
            case 'X':
                classes.push('hit');
                break;
            case '#':
                classes.push('ship');
                break;
            case '0':
                classes.push('miss');
                break;
            default:
                classes.push('empty');
        }
    }
    return (
        <button className={classes.join(' ')} onClick={props.onClick}/>
    );
}

export default Square;
