import React from 'react';
import './Game.css';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

export default class Game extends React.PureComponent {
    static propTypes = {
        id: PropTypes.number,
        onClick: PropTypes.func,
        opponentId: PropTypes.number,
        playerId: PropTypes.number,
        status: PropTypes.string
    };

    handleClick = () => {
        const {onClick, opponentId, id} = this.props;
        onClick(id, opponentId);
    };

    render() {
        const {id, opponentId, status} = this.props;
        return (
            <div className="game" onClick={this.handleClick}>
                <h3>Game ID: {id}</h3>
                <h4>Opponent ID: {opponentId}</h4>
                <h4>Status: {status}</h4>
                <Link className="my-link" to={`/game/${id}`}>Game status</Link>
            </div>
        );
    }
}
