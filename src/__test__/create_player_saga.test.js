import {testSaga} from 'redux-saga-test-plan';
import {createPlayer} from '../api/forms';
import {createPlayerError, createPlayerSuccess} from '../redux/forms/actions';
import {createPlayerSaga} from '../saga/sagas';

describe('create player saga', () => {
    it('should create player', () => {
        const fakeAction = {email: 'player@ag04.com', name: 'playerName'};
        const saga = testSaga(createPlayerSaga, fakeAction);
        saga.next().call(createPlayer, fakeAction.name, fakeAction.email);
        saga.next().put(createPlayerSuccess());
        saga.next().isDone();
    });

    it('should throw error', () => {
        const error = new Error('My Error');
        const fakeAction = {email: 'player@ag04.com', name: 'playerName'};
        const saga = testSaga(createPlayerSaga, fakeAction);
        saga.next().throw(error)
            .put(createPlayerError(error))
            .next()
            .isDone();
    });
});
