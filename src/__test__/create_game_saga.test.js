import {testSaga} from 'redux-saga-test-plan';
import {createGame} from '../api/forms';
import {createGameError, createGameSuccess} from '../redux/forms/actions';
import {createGameSaga} from '../saga/sagas';

describe('create game saga', () => {
    it('should create game', () => {
        const fakeAction = {email: 'player@ag04.com', name: 'playerName'};
        const saga = testSaga(createGameSaga, fakeAction);
        saga.next().call(createGame, fakeAction.challengerId, fakeAction.opponentId);
        saga.next().put(createGameSuccess());
        saga.next().isDone();
    });

    it('should throw error', () => {
        const error = new Error('My Error');
        const fakeAction = {email: 'player@ag04.com', name: 'playerName'};
        const saga = testSaga(createGameSaga, fakeAction);
        saga.next().throw(error)
            .put(createGameError(error))
            .next()
            .isDone();
    });
});
