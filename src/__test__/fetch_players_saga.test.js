import {testSaga} from 'redux-saga-test-plan';
import {fetchPlayerList} from '../api/players';
import {fetchPlayersError, setPlayers} from '../redux/players/actions';
import {fetchPlayersSaga} from '../saga/sagas';

describe('fetch players saga', () => {
    it('should fetch players', () => {
        const fakePlayers = [{email: 'pperic@ag04.com', id: 1, name: 'pperic'}, {
            email: 'mmiric@ag04.com',
            id: 2,
            name: 'mmiric'
        }];
        const fakeResponse = {data: {players: fakePlayers}};
        const saga = testSaga(fetchPlayersSaga);
        saga.next().call(fetchPlayerList);
        saga.next(fakeResponse).put(setPlayers(fakePlayers));
        saga.next().isDone();
    });

    it('should throw error', () => {
        const error = new Error('My Error');
        const saga = testSaga(fetchPlayersSaga);
        saga.next().throw(error)
            .put(fetchPlayersError(error))
            .next()
            .isDone();
    });
});
