import {testSaga} from 'redux-saga-test-plan';
import {fireSalvoSaga} from '../saga/sagas';
import {fireSalvo} from '../api/games';
import {fireSalvoSuccess} from '../redux/games/actions';

describe('fire salvo saga', () => {
    it('should fire salvo', () => {
        const fakeSalvoResult = {
            '2xG': 'HIT',
            '5xJ': 'KILL',
            '7xA': 'MISS'
        };
        const fakeAction = {gameId: 1, playerId: 1, salvo: ['2xG', '5xJ', '7xA']};
        const fakeResponse = {data: {salvo: fakeSalvoResult}};
        const saga = testSaga(fireSalvoSaga, fakeAction);
        saga.next().call(fireSalvo, fakeAction.playerId, fakeAction.gameId, fakeAction.salvo);
        saga.next(fakeResponse).put(fireSalvoSuccess(fakeResponse.data.salvo));
        saga.next().isDone();
    });

    it('should throw error', () => {
        const fakeAction = {gameId: 1, playerId: 1, salvo: ['2xG', '5xJ', '7xA']};
        const error = new Error('My Error');
        const saga = testSaga(fireSalvoSaga, fakeAction);
        saga.next().throw(error)
            .next()
            .isDone();
    });
});
