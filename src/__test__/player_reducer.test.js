import expect from 'expect';
import * as actions from '../redux/players/actions';
import {initialState, playersReducer} from '../redux/players/reducer';

describe('player reducer', () => {
    it('should handle FETCH_PLAYERS_SUCCESS', () => {
        const players = [{email: 'pperic@ag04.com', id: 1, name: 'pperic'},
            {email: 'mmiric@ag04.com', id: 2, name: 'mmiric'}];
        expect(playersReducer(initialState, actions.setPlayers(players)))
            .toEqual({...initialState, players: players});
    });
    it('should handle FETCH_PLAYERS_ERROR', () => {
        expect(playersReducer(initialState, actions.fetchPlayersError()))
            .toEqual({...initialState, playersFetchError: 'Error while fetching players!'});
    });
});
