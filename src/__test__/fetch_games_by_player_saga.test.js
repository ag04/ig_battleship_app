import {testSaga} from 'redux-saga-test-plan';
import {fetchGameList} from '../api/games';
import {fetchGamesByPlayerError, setGames} from '../redux/games/actions';
import {fetchGamesByPlayerSaga} from '../saga/sagas';

describe('fetch games by player saga', () => {
    it('should fetch games', () => {
        const fakeGames = [{gameId: 1, opponentId: 2, status: 'IN_PROGRESS'}, {
            gameId: 2,
            opponentId: 2,
            status: 'WON'
        }];
        const fakeAction = {playerId: 1};
        const fakeResponse = {data: {games: fakeGames}};
        const saga = testSaga(fetchGamesByPlayerSaga, fakeAction);
        saga.next().call(fetchGameList, fakeAction.playerId);
        saga.next(fakeResponse).put(setGames(fakeGames, fakeAction.playerId));
        saga.next().isDone();
    });

    it('should throw error', () => {
        const fakeAction = {playerId: 1};
        const error = new Error('My Error');
        const saga = testSaga(fetchGamesByPlayerSaga, fakeAction);
        saga.next().throw(error)
            .put(fetchGamesByPlayerError(error))
            .next()
            .isDone();
    });
});
