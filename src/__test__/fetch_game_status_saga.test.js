import {testSaga} from 'redux-saga-test-plan';
import {fetchGameStatusSaga} from '../saga/sagas';
import {fetchGameStatusError, setGameStatus} from '../redux/games/actions';
import {fetchGameStatus} from '../api/games';

describe('fetch game status saga', () => {
    it('should fetch game status', () => {
        const fakeAction = {gameId: 1, opponentId: 2, playerId: 1};
        const fakeData = {
            game: {won: 2},
            opponent: {
                autopilot: false,
                board: [
                    '0.00000000',
                    '0X00000000',
                    '00XXXX0000',
                    'XX000.00X0',
                    '000X000X00',
                    '000X00.0X0',
                    '000X0X0.X0',
                    '00X00X00X.',
                    '00X000X000',
                    '00000.0000'
                ],
                playerId: 3,
                remainingShips: 0
            },
            self: {
                autopilot: false,
                board: [
                    'X0.0XXX.00',
                    '..000..X00',
                    '.000X00000',
                    '000.#00#.0',
                    '0..#00..00',
                    '000X.00.0.',
                    '..0X.00.X.',
                    '.00X000.#0',
                    '#0X000X0X0',
                    '#00.0.X.0.'
                ],
                playerId: 2,
                remainingShips: 5
            }
        };
        const fakeResponse = {data: fakeData};
        const saga = testSaga(fetchGameStatusSaga, fakeAction);
        saga.next().call(fetchGameStatus, fakeAction.playerId, fakeAction.gameId);
        saga.next(fakeResponse).put(setGameStatus(fakeData));
        saga.next().isDone();
    });

    it('should throw error', () => {
        const fakeAction = {gameId: 1, opponentId: 2, playerId: 1};
        const error = new Error('My Error');
        const saga = testSaga(fetchGameStatusSaga, fakeAction);
        saga.next().throw(error)
            .put(fetchGameStatusError(error))
            .next()
            .isDone();
    });
});
