import expect from 'expect';
import * as actions from '../redux/games/actions';
import {gamesReducer, initialState} from '../redux/games/reducer';

describe('game reducer', () => {
    it('should handle FETCH_GAMES_BY_PLAYER_SUCCESS', () => {
        const playerId = 1;
        const games = [{gameId: 1, opponentId: 2, status: 'LOST'}];
        expect(gamesReducer(initialState, actions.setGames(games, playerId)))
            .toEqual({...initialState, games: games, gamesByPlayerFetchError: '', selectedPlayer: playerId});
    });
    it('should handle FETCH_GAMES_BY_PLAYER_ERROR', () => {
        expect(gamesReducer(initialState, actions.fetchGamesByPlayerError()))
            .toEqual({...initialState, gamesByPlayerFetchError: 'Error while fetching games!'});
    });
    it('should handle SET_GAME_STATUS_PARAMS', () => {
        const game = 1;
        const opponent = 2;
        expect(gamesReducer(initialState, actions.setGameStatusParams(game, opponent)))
            .toEqual({...initialState, selectedGame: game, selectedOpponent: opponent});
    });
    it('should handle FETCH_GAME_STATUS_SUCCESS', () => {
        const fakeAction = {
            game: {won: 2},
            opponent: {
                autopilot: false,
                board: [
                    '0.00000000',
                    '0X00000000',
                    '00XXXX0000',
                    'XX000.00X0',
                    '000X000X00',
                    '000X00.0X0',
                    '000X0X0.X0',
                    '00X00X00X.',
                    '00X000X000',
                    '00000.0000'
                ],
                playerId: 3,
                remainingShips: 0
            },
            self: {
                autopilot: false,
                board: [
                    'X0.0XXX.00',
                    '..000..X00',
                    '.000X00000',
                    '000.#00#.0',
                    '0..#00..00',
                    '000X.00.0.',
                    '..0X.00.X.',
                    '.00X000.#0',
                    '#0X000X0X0',
                    '#00.0.X.0.'
                ],
                playerId: 2,
                remainingShips: 5
            }
        };
        expect(gamesReducer(initialState, actions.setGameStatus(fakeAction)))
            .toEqual({
                ...initialState,
                gameStatus: fakeAction || {},
                gameStatusFetchError: '',
                gameStatusFetchSuccess: 'Game status fetch success.'
            });
    });
    it('should handle FETCH_GAME_STATUS_ERROR', () => {
        expect(gamesReducer(initialState, actions.fetchGameStatusError()))
            .toEqual({
                ...initialState,
                gameStatus: {},
                gameStatusFetchError: 'Error while fetching game status!',
                gameStatusFetchSuccess: ''
            });
    });
    it('should handle GAME_STATUS_CLEAR_MESSAGES', () => {
        expect(gamesReducer(initialState, actions.clearGameStatusMessages()))
            .toEqual({...initialState, gameStatusFetchError: '', gameStatusFetchSuccess: ''});
    });
    it('should handle FIRE_SALVO_SUCCESS', () => {
        const fakeSalvoResult = {
            '2xG': 'HIT',
            '5xJ': 'KILL',
            '7xA': 'MISS'
        };
        expect(gamesReducer(initialState, actions.fireSalvoSuccess(fakeSalvoResult)))
            .toEqual({
                ...initialState,
                fireSalvoError: '',
                fireSalvoSuccess: 'Salvo successfully fired.',
                salvoResult: fakeSalvoResult,
                selectedOpponent: initialState.selectedPlayer,
                selectedPlayer: initialState.selectedOpponent
            });
    });
    it('should handle FIRE_SALVO_ERROR', () => {
        expect(gamesReducer(initialState, actions.fireSalvoError()))
            .toEqual({
                ...initialState,
                fireSalvoError: 'Error while firing salvo!',
                fireSalvoSuccess: '',
                salvoShots: []
            });
    });
    it('should handle TOGGLE_SQUARE_FOR_SALVO', () => {
        const squareId1 = 1;
        expect(gamesReducer(initialState, actions.toggleSquareForSalvo(squareId1)))
            .toEqual({...initialState, salvoShots: [...initialState.salvoShots, squareId1]});
        const squareId2 = 2;
        const newState = Object.assign({}, {...initialState, salvoShots: [...initialState.salvoShots, squareId2]});
        expect(gamesReducer(newState, actions.toggleSquareForSalvo(squareId2)))
            .toEqual({
                ...initialState,
                salvoShots: [...newState.salvoShots.filter((square) => square !== squareId2)]
            });
    });
});
