import expect from 'expect';
import {formsReducer, initialState} from '../redux/forms/reducer';
import * as actions from '../redux/forms/actions';

describe('form reducer', () => {
    it('should handle CREATE_PLAYER_SUCCESS', () => {
        expect(formsReducer(initialState, actions.createPlayerSuccess()))
            .toEqual({...initialState, playerCreateError: '', playerCreateSuccess: 'Player successfully created.'});
    });
    it('should handle CREATE_PLAYER_ERROR', () => {
        expect(formsReducer(initialState, actions.createPlayerError()))
            .toEqual({...initialState, playerCreateError: 'Error while creating player!', playerCreateSuccess: ''});
    });
    it('should handle CLEAR_PLAYER_FORM_STATE', () => {
        expect(formsReducer(initialState, actions.clearPlayerFormState()))
            .toEqual({...initialState, playerCreateError: '', playerCreateSuccess: ''});
    });
    it('should handle CREATE_GAME_SUCCESS', () => {
        expect(formsReducer(initialState, actions.createGameSuccess()))
            .toEqual({...initialState, gameCreateError: '', gameCreateSuccess: 'Game successfully created.'});
    });
    it('should handle CREATE_GAME_ERROR', () => {
        expect(formsReducer(initialState, actions.createGameError()))
            .toEqual({...initialState, gameCreateError: 'Error while creating game!', gameCreateSuccess: ''});
    });
    it('should handle CLEAR_GAME_FORM_STATE', () => {
        expect(formsReducer(initialState, actions.clearGameFormState()))
            .toEqual({...initialState, gameCreateError: '', gameCreateSuccess: ''});
    });
});
