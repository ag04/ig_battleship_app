export const convertLocationToInteger = (location) => {
    let xCord, yCord;
    if (location.length === 3) {
        xCord = location.charAt(0) - 1;
        yCord = location.charCodeAt(2) - 65;
    } else {
        xCord = 9;
        yCord = location.charCodeAt(3) - 65;
    }
    return xCord * 10 + yCord;
};

export const convertIntegersToLocations = (squares) => squares.map((shot) => {
    const shotX = Math.floor(shot / 10) + 1;
    const shotY = shot % 10;
    return `${shotX}x${String.fromCharCode(shotY + 65)}`;
});
