import {
    FETCH_GAME_STATUS_ERROR, FETCH_GAME_STATUS_SUCCESS, FETCH_GAMES_BY_PLAYER_ERROR, FETCH_GAMES_BY_PLAYER_SUCCESS,
    FIRE_SALVO_ERROR, FIRE_SALVO_SUCCESS, GAME_STATUS_CLEAR_MESSAGES, SET_GAME_STATUS_PARAMS, TOGGLE_SQUARE_FOR_SALVO
} from './actions';

export const initialState = {
    fireSalvoError: '',
    fireSalvoSuccess: '',
    games: [],
    gamesByPlayerFetchError: '',
    gameStatus: {},
    gameStatusFetchError: '',
    gameStatusFetchSuccess: '',
    salvoResult: {},
    salvoShots: [],
    selectedGame: 0,
    selectedOpponent: 0,
    selectedPlayer: 0
};

export function gamesReducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_GAMES_BY_PLAYER_SUCCESS:
            return {...state, games: action.payload || [], gamesByPlayerFetchError: '', selectedPlayer: action.playerId};
        case FETCH_GAMES_BY_PLAYER_ERROR:
            return {...state, gamesByPlayerFetchError: 'Error while fetching games!'};
        case SET_GAME_STATUS_PARAMS:
            return {...state, selectedGame: action.gameId, selectedOpponent: action.opponentId};
        case FETCH_GAME_STATUS_SUCCESS:
            return {
                ...state,
                fireSalvoError: '',
                fireSalvoSuccess: '',
                gameStatus: action.payload || {},
                gameStatusFetchError: '',
                gameStatusFetchSuccess: 'Game status fetch success.',
                salvoResult: {},
                salvoShots: []
            };
        case FETCH_GAME_STATUS_ERROR:
            return {...state, gameStatus: {}, gameStatusFetchError: 'Error while fetching game status!', gameStatusFetchSuccess: ''};
        case GAME_STATUS_CLEAR_MESSAGES:
            return {...state, gameStatusFetchError: '', gameStatusFetchSuccess: ''};
        case FIRE_SALVO_SUCCESS:
            return {
                ...state,
                fireSalvoError: '',
                fireSalvoSuccess: 'Salvo successfully fired.',
                salvoResult: action.salvo,
                selectedOpponent: state.selectedPlayer,
                selectedPlayer: state.selectedOpponent
            };
        case FIRE_SALVO_ERROR:
            return {...state, fireSalvoError: 'Error while firing salvo!', fireSalvoSuccess: '', salvoShots: []};
        case TOGGLE_SQUARE_FOR_SALVO:
            if (state.salvoShots.includes(action.squareId)) {
                return {...state, salvoShots: [...state.salvoShots.filter((square) => square !== action.squareId)]};
            }
            return {...state, salvoShots: [...state.salvoShots, action.squareId]};
        default:
            return state;
    }
}
