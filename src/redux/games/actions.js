export const FETCH_GAMES_BY_PLAYER = 'FETCH_GAMES_BY_PLAYER';
export const FETCH_GAMES_BY_PLAYER_SUCCESS = 'FETCH_GAMES_BY_PLAYER_SUCCESS';
export const FETCH_GAMES_BY_PLAYER_ERROR = 'FETCH_GAMES_BY_PLAYER_ERROR';

export const FETCH_GAME_STATUS = 'FETCH_GAME_STATUS';
export const FETCH_GAME_STATUS_SUCCESS = 'FETCH_GAME_STATUS_SUCCESS';
export const FETCH_GAME_STATUS_ERROR = 'FETCH_GAME_STATUS_ERROR';

export const SET_GAME_STATUS_PARAMS = 'SET_GAME_STATUS_PARAMS';
export const GAME_STATUS_CLEAR_MESSAGES = 'GAME_STATUS_CLEAR_MESSAGES';

export const FIRE_SALVO = 'FIRE_SALVO';
export const FIRE_SALVO_SUCCESS = 'FIRE_SALVO_SUCCESS';
export const FIRE_SALVO_ERROR = 'FIRE_SALVO_ERROR';

export const TOGGLE_SQUARE_FOR_SALVO = 'TOGGLE_SQUARE_FOR_SALVO';

export const fetchGamesByPlayer = (playerId) => ({playerId: playerId, type: FETCH_GAMES_BY_PLAYER});
export const setGames = (games, playerId) => ({
    payload: games,
    playerId: playerId,
    type: FETCH_GAMES_BY_PLAYER_SUCCESS
});
export const fetchGamesByPlayerError = () => ({type: FETCH_GAMES_BY_PLAYER_ERROR});

export const fetchGameStatus = (playerId, gameId) => ({gameId, playerId, type: FETCH_GAME_STATUS});
export const setGameStatus = (gameStatus) => ({
    payload: gameStatus,
    type: FETCH_GAME_STATUS_SUCCESS
});
export const fetchGameStatusError = () => ({type: FETCH_GAME_STATUS_ERROR});

export const setGameStatusParams = (gameId, opponentId) => ({gameId, opponentId, type: SET_GAME_STATUS_PARAMS});
export const clearGameStatusMessages = () => ({type: GAME_STATUS_CLEAR_MESSAGES});

export const fireSalvo = (playerId, gameId, salvo) => ({gameId, playerId, salvo, type: FIRE_SALVO});
export const fireSalvoSuccess = (salvo) => ({salvo, type: FIRE_SALVO_SUCCESS});
export const fireSalvoError = () => ({type: FIRE_SALVO_ERROR});

export const toggleSquareForSalvo = (squareId) => ({squareId, type: TOGGLE_SQUARE_FOR_SALVO});
