import createSagaMiddleware from 'redux-saga';
import {applyMiddleware, combineReducers, createStore} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension/developmentOnly';
import {reducer as toastrReducer} from 'react-redux-toastr';
import {mainSaga} from '../saga/main-saga';
import {playersReducer} from './players/reducer';
import {gamesReducer} from './games/reducer';
import {formsReducer} from './forms/reducer';

const rootReducer = combineReducers({
    forms: formsReducer,
    games: gamesReducer,
    players: playersReducer,
    toastr: toastrReducer
});

const sagaMiddleware = createSagaMiddleware();

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(sagaMiddleware)));

sagaMiddleware.run(mainSaga);

export default store;
