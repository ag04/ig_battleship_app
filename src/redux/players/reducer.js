import {FETCH_PLAYERS_ERROR, FETCH_PLAYERS_SUCCESS} from './actions';

export const initialState = {
    players: [],
    playersFetchError: ''
};

export function playersReducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_PLAYERS_SUCCESS:
            return {...state, players: action.payload, playersFetchError: ''};
        case FETCH_PLAYERS_ERROR:
            return {...state, playersFetchError: 'Error while fetching players!'};
        default:
            return state;
    }
}
