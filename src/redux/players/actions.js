export const FETCH_PLAYERS = 'FETCH_PLAYERS';
export const FETCH_PLAYERS_SUCCESS = 'FETCH_PLAYERS_SUCCESS';
export const FETCH_PLAYERS_ERROR = 'FETCH_PLAYERS_ERROR';

export const fetchPlayers = () => ({type: FETCH_PLAYERS});

export const setPlayers = (players) => ({
    payload: players,
    type: FETCH_PLAYERS_SUCCESS
});

export const fetchPlayersError = () => ({type: FETCH_PLAYERS_ERROR});
