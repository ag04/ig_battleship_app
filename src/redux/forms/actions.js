export const CREATE_PLAYER = 'CREATE_PLAYER';
export const CREATE_PLAYER_SUCCESS = 'CREATE_PLAYER_SUCCESS';
export const CREATE_PLAYER_ERROR = 'CREATE_PLAYER_ERROR';

export const CLEAR_PLAYER_FORM_STATUS_MESSAGES = 'CLEAR_PLAYER_FORM_STATUS_MESSAGES';

export const CREATE_GAME = 'CREATE_GAME';
export const CREATE_GAME_SUCCESS = 'CREATE_GAME_SUCCESS';
export const CREATE_GAME_ERROR = 'CREATE_GAME_ERROR';

export const CLEAR_GAME_FORM_STATUS_MESSAGES = 'CLEAR_GAME_FORM_STATUS_MESSAGES';

export const createPlayer = (name, email) => ({
    email: email,
    name: name,
    type: CREATE_PLAYER
});

export const createPlayerSuccess = () => ({type: CREATE_PLAYER_SUCCESS});

export const createPlayerError = () => ({type: CREATE_PLAYER_ERROR});

export const clearPlayerFormState = () => ({type: CLEAR_PLAYER_FORM_STATUS_MESSAGES});

export const createGame = (challengerId, opponentId) => ({
    challengerId: challengerId,
    opponentId: opponentId,
    type: CREATE_GAME
});

export const createGameSuccess = () => ({type: CREATE_GAME_SUCCESS});

export const createGameError = () => ({type: CREATE_GAME_ERROR});

export const clearGameFormState = () => ({type: CLEAR_GAME_FORM_STATUS_MESSAGES});
