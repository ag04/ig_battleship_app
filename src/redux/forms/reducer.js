import {
    CLEAR_GAME_FORM_STATUS_MESSAGES, CLEAR_PLAYER_FORM_STATUS_MESSAGES, CREATE_GAME_ERROR,
    CREATE_GAME_SUCCESS, CREATE_PLAYER_ERROR, CREATE_PLAYER_SUCCESS
} from './actions';

export const initialState = {
    gameCreateError: '',
    gameCreateSuccess: '',
    playerCreateError: '',
    playerCreateSuccess: ''
};

export function formsReducer(state = initialState, action) {
    switch (action.type) {
        case CREATE_PLAYER_SUCCESS:
            return {...state, playerCreateError: '', playerCreateSuccess: 'Player successfully created.'};
        case CREATE_PLAYER_ERROR:
            return {...state, playerCreateError: 'Error while creating player!', playerCreateSuccess: ''};
        case CLEAR_PLAYER_FORM_STATUS_MESSAGES:
            return {...state, playerCreateError: '', playerCreateSuccess: ''};
        case CREATE_GAME_SUCCESS:
            return {...state, gameCreateError: '', gameCreateSuccess: 'Game successfully created.'};
        case CREATE_GAME_ERROR:
            return {...state, gameCreateError: 'Error while creating game!', gameCreateSuccess: ''};
        case CLEAR_GAME_FORM_STATUS_MESSAGES:
            return {...state, gameCreateError: '', gameCreateSuccess: ''};
        default:
            return {...state};
    }
}
